package Aula08.Entities;

public class Triangle {
    public double lado1, lado2, lado3;

    public double calculateArea(double lado1, double lado2, double lado3) {
        double p = (lado1 + lado2 + lado3) / 2;
        return Math.sqrt(p * (p-lado1) * (p-lado2) * (p-lado3));
    }
}
