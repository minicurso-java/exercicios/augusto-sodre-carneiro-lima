package Aula08.Entities;

public class Rectangle {
    public double Width;
    public double Height;

    public double Area(double width, double height){
        return width * height;
    };

    public double Perimeter(double width, double height){
        return (2.0 * width) + (2.0 * height);
    }

    public double Diagonal(double width, double height){
        return Math.sqrt((width * width) + (height * height));
    }
}
