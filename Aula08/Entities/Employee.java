package Aula08.Entities;

public class Employee {
    public String name;
    public double grossSalary;
    public double tax;

    public double netSalary(){
        return Employee.this.grossSalary - Employee.this.tax;
    }

    public void increaseSalary(double percentage){
        Employee.this.grossSalary *= 1 + percentage/100;
    }
}
