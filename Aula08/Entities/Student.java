package Aula08.Entities;

public class Student {
    public String name;
    public double nota1;
    public double nota2;
    public double nota3;
    public double media;
    public double minima;

    public boolean approved(double n1, double n2, double n3) {
        int p1 = 30;
        int p2 = 35;
        int p3 = 35;
        double notaMin = ((100.0 * p1) + (100.0 * p2) + (100.0 * p3) / (p1 + p2 + p3)) * 0.6;
        double notaTirada = (n1 * p1) + (n2 * p2) + (n3 * p3) / (p1 + p2 + p3);
        Student.this.minima = notaMin;
        Student.this.media = notaTirada;
        if (notaMin > notaTirada) {
            return false;
        } else{
            return true;
        }
    }
}
