package Aula08.ParaEntrega;

import Aula08.Entities.Student;

import java.util.Scanner;

public class Ex03 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Student aluno = new Student();
        aluno.name = sc.nextLine();
        aluno.nota1 = sc.nextDouble();
        aluno.nota2 = sc.nextDouble();
        aluno.nota3 = sc.nextDouble();
        if (aluno.approved(aluno.nota1, aluno.nota2, aluno.nota3)){
            System.out.println("Aprovado");
            System.out.println("Nota final:" + aluno.media);
        } else{
            System.out.println("Reprovado");
            System.out.println("Nota final:" + aluno.media);
            System.out.println("Faltou" + (aluno.minima - aluno.media));
        }
        System.out.println("Nota final: " + aluno.media);


    }
}
