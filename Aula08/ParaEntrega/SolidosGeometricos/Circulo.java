package Aula08.ParaEntrega.SolidosGeometricos;

public class Circulo extends FigurasGeometricas{
    double raio;

    @Override
    double calcArea() {
        return Math.PI * Math.pow(raio, 2);
    }

    double calcPerimetro(){
        return 2.0 * Math.PI * raio;
    }
}
