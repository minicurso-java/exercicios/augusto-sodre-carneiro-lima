package Aula08.ParaEntrega.SolidosGeometricos;

public class Quadrado extends FigurasGeometricas {
    double lado;

    @Override
    double calcArea() {
        return Math.pow(lado,2);
    }

    @Override
    double calcPerimetro() {
        return 4.0 * lado;
    }
}
