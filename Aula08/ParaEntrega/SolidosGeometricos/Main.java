package Aula08.ParaEntrega.SolidosGeometricos;

import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        //Instâncias das classes
        Circulo circulo = new Circulo();
        Quadrado quadrado = new Quadrado();
        Retangulo retangulo = new Retangulo();
        Triangulo triangulo = new Triangulo();

        System.out.print("\nSólidos geométricos!\n\n");

        System.out.print("\nCírculo!\n");
        System.out.print("Digite o nome do círculo: ");
        circulo.nome = sc.nextLine();
        System.out.print("Digite o raio do círculo: ");
        circulo.raio = sc.nextDouble();
        System.out.printf("\n%s: \n-Área: %.3f \n-Perímetro: %.3f\n", circulo.nome, circulo.calcArea(), circulo.calcPerimetro());

        System.out.print("\nQuadrado!\n");
        System.out.print("Digite o nome do quadrado: ");
        quadrado.nome = sc.next();
        System.out.print("Digite o lado do quadrado: ");
        quadrado.lado = sc.nextDouble();
        System.out.printf("\n%s: \n-Área: %.3f \n-Perímetro: %.3f\n", quadrado.nome, quadrado.calcArea(), quadrado.calcPerimetro());

        System.out.print("\nRetângulo!\n");
        System.out.print("Digite o nome do retângulo: ");
        retangulo.nome = sc.next();
        System.out.print("Digite a altura do retângulo: ");
        retangulo.altura = sc.nextDouble();
        System.out.print("Digite a largura do retângulo: ");
        retangulo.largura = sc.nextDouble();
        System.out.printf("\n%s: \n-Área: %.3f \n-Perímetro: %.3f\n", retangulo.nome, retangulo.calcArea(), retangulo.calcPerimetro());

        System.out.print("\nTriângulo!\n");
        System.out.print("Digite o nome do triângulo: ");
        triangulo.nome = sc.next();
        for (int i = 0; i < 3; i++){
            System.out.printf("Digite o lado %d do triângulo: ", i + 1);
            triangulo.lados[i] = sc.nextDouble();
        }
        System.out.printf("\n%s: \n-Área: %.3f \n-Perímetro: %.3f\n", triangulo.nome, triangulo.calcArea(), triangulo.calcPerimetro());
        System.out.print("\nBye bye!\n");
    }
}
