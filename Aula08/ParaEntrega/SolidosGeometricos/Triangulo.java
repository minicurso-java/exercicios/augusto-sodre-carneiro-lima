package Aula08.ParaEntrega.SolidosGeometricos;

public class Triangulo extends FigurasGeometricas {
    double[] lados = new double[3];

    //Usando a fórmula de Heron!
    @Override
    double calcArea() {
        double p = calcPerimetro() / 2.0; //Semiperímetro
        return Math.sqrt(p * (p - lados[0]) * (p - lados[1]) * (p - lados[2]));
    }

    @Override
    double calcPerimetro() {
        return (lados[0] + lados[1] + lados[2]);
    }
}
