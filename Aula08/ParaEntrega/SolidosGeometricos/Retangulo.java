package Aula08.ParaEntrega.SolidosGeometricos;

public class Retangulo extends FigurasGeometricas{
    double altura;
    double largura;

    @Override
    double calcArea() {
        return altura * largura;
    }

    @Override
    double calcPerimetro() {
        return (altura * 2.0) + (largura * 2.0);
    }
}
