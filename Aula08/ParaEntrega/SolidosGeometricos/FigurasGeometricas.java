package Aula08.ParaEntrega.SolidosGeometricos;

public abstract class FigurasGeometricas {
    String nome;

    abstract double calcArea();

    abstract double calcPerimetro();
}
