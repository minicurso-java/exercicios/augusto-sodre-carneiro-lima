package Aula08.EmSala;

import Aula08.Entities.Employee;

import java.util.Scanner;

public class Ex02 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Employee funcionario = new Employee();
        funcionario.name = sc.nextLine();
        funcionario.grossSalary = sc.nextDouble();
        funcionario.tax = sc.nextDouble();
        System.out.println("Funcionário: " + funcionario.name + ", " + funcionario.netSalary());
        System.out.println("Qual a porcentagem do aumento?");
        double aumento = sc.nextDouble();
        funcionario.increaseSalary(aumento);
        System.out.println("Atualização: " + funcionario.name + ", " + funcionario.netSalary());
    }
}
