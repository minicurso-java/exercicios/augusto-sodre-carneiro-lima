package Aula08.EmSala;

import Aula08.Entities.Rectangle;

import java.util.Scanner;

public class Ex01 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Rectangle x = new Rectangle();
        x.Height = sc.nextDouble();
        x.Width = sc.nextDouble();
        double areaX = x.Area(x.Width, x.Height);
        double perimetroX = x.Perimeter(x.Width, x.Height);
        double diagonalX = x.Diagonal(x.Width, x.Height);
        System.out.println("Area: " + areaX);
        System.out.println("Perimetro: " + perimetroX);
        System.out.println("Diagonal: " + diagonalX);
    }
}
