package Aula04;

import java.util.Scanner;

public class Ex04 {
    public static void main(String[] args) {
        int nota1 = 1, nota2 = 2, nota3 = 3;
        int peso1 = 5, peso2 = 3, peso3 = 2;
        String answer = " ";
        Scanner sc = new Scanner(System.in);
        System.out.print("Digite [A] ou [a] para Média Aritmética e [P] ou [p] para Média Ponderada: ");
        answer = sc.next();
        answer = answer.trim();
        answer = answer.toLowerCase();
        switch (answer){
            case "a":
                System.out.println("Média Aritmética: " + mediaAritmetica(nota1, nota2, nota3));
            break;

            case "p":
                System.out.println("Média Ponderada: " + mediaPonderada(nota1, nota2, nota3, peso1, peso2, peso3));
            break;
        }
        sc.close();
    }

    public static int mediaAritmetica (int nota1, int nota2, int nota3){
        int somaElementos = nota1 + nota2 + nota3;
        return somaElementos/3;
    }

    public static double mediaPonderada (int nota1, int nota2, int nota3, int peso1, int peso2, int peso3){
        int somaPesos = peso1 + peso2 + peso3;
        double notaComPeso = nota1 * peso1 + nota2 * peso2 + nota3 * peso3;
        return notaComPeso/somaPesos;
    }
}
