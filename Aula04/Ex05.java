package Aula04;

import java.util.Scanner;

public class Ex05 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Digite uma string: ");
        String userString = sc.nextLine();
        System.out.println("Digite uma letra para remover: ");
        char letra = sc.next().charAt(0);
        System.out.println("Digite uma letra para substituir: ");
        char letraNova = sc.next().charAt(0);
        userString = userString.replace(letra, letraNova);
        System.out.println(userString);
        sc.close();
    }
}
