package Aula04;

import java.util.Scanner;

public class Ex06 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Digite uma string: ");
        String userString = sc.nextLine();
        int i = 0;
        while (i < userString.length()){
            switch (userString.toLowerCase().charAt(i)) {
                case 'a', 'e', 'i', 'o', 'u', 'á', 'ã', 'â':
                    userString = userString.substring(0, i) + userString.substring(i + 1);
                    i = 0;
                    break;
                default: i++;
            }
        }
        userString = userString.trim();
        System.out.println(userString);
        sc.close();
    }
}
