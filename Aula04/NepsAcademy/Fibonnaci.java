package Aula04.NepsAcademy;

import java.util.Scanner;

public class Fibonnaci {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int num1 = 1, num2 = 1, numAtual;
        int limit = sc.nextInt();
        System.out.print(0 + " ");
        System.out.print(num1 + " ");
        System.out.print(num2 + " ");
        for(int i = 0; i < limit - 3; i++){
            numAtual = num1 + num2;
            System.out.print(numAtual + " ");
            num1 = num2;
            num2 = numAtual;
        }
        sc.close();
    }
}
