package Aula04.NepsAcademy;

import java.util.Scanner;

public class BasqueteRobo {

    public static void main (String[] args){
        Scanner sc = new Scanner(System.in);
        int distancia = sc.nextInt();
        if (distancia <= 800){
            System.out.println("1");
        } else if (distancia > 800 && distancia <= 1400) {
            System.out.println("2");
        } else {
            System.out.println("3");
        }
        sc.close();
    }
}