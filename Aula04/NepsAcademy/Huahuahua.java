package Aula04.NepsAcademy;

import java.util.Arrays;
import java.util.Scanner;

public class Huahuahua {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        String userInput = sc.nextLine();
        int length = userInput.length();
        char[] inputFrontwards = new char[length], inputBackwards = new char[length];
        int indexFrontwards = 0, indexBackwards = 0;
        //Preenchendo array frente com as vogais
        for (int i = 0; i < length; i++){
            switch (userInput.toLowerCase().charAt(i)){
                case 'a', 'e', 'i', 'o', 'u':
                    inputFrontwards[indexFrontwards] = userInput.charAt(i);
                    indexFrontwards++;
                break;
            }
        }
        //Preenchendo array verso com as vogais
        for (int j = length - 1; j >= 0; j--){
            switch (userInput.toLowerCase().charAt(j)){
                case 'a', 'e', 'i', 'o', 'u':
                    inputBackwards[indexBackwards] = userInput.charAt(j);
                    indexBackwards++;
                    break;
            }
        }
        //Comparação entre 2 arrays: frente e verso
        if (Arrays.equals(inputFrontwards, inputBackwards)){
            System.out.println("S");
        } else {
            System.out.println("N");
        }
    }
}
