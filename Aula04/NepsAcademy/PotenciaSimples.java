package Aula04.NepsAcademy;

import java.util.Scanner;

public class PotenciaSimples {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        double x = sc.nextDouble(), y = sc.nextDouble();
        System.out.printf("%.4f", Math.pow(x, y));
    }
}
