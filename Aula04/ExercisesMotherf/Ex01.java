package Aula04.ExercisesMotherf;

import java.util.Scanner;

public class Ex01 {
    public static void main(String[] args) {
        int num1, num2;
        Scanner sc = new Scanner(System.in);
        num1 = sc.nextInt();
        num2 = sc.nextInt();
        System.out.println(multiplica(num1, num2));
        sc.close();
    }

    public static int multiplica(int num1, int num2) {
        return num1 * num2;
    }
}
