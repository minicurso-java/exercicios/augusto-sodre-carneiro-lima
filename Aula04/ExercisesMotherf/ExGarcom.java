package Aula04.ExercisesMotherf;

import java.util.Scanner;

public class ExGarcom {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int numBandejas, coposQuebrados = 0;
        System.out.print("Número de bandejas: ");
        numBandejas = sc.nextInt();
        for (int i = 0; i < numBandejas; i++) {
            int numCopos, numLatas;
            System.out.println("Número de latas: ");
            numLatas = sc.nextInt();
            System.out.println("Número de copos: ");
            numCopos = sc.nextInt();

            if (numLatas > numCopos) {
                coposQuebrados += numCopos;
            }
        }
        System.out.println(coposQuebrados);
        sc.close();
    }
}
