package Aula04.ExercisesMotherf;

import java.util.Arrays;
import java.util.Scanner;

public class Ex03 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int numElementos = sc.nextInt();
        int[] elementos = new int[numElementos];
        int[] pesoElementos = new int[numElementos];

        for (int i = 0; i < numElementos; i++){
            System.out.print("Digite um valor: ");
            elementos[i] += sc.nextInt();
        }

        int somaElementos = Arrays.stream(elementos).sum();

        for (int j = 0; j < numElementos; j++){
            System.out.println("Digite um peso: ");
            pesoElementos[j] = sc.nextInt();
        }

        System.out.println("Média Aritmética: " + mediaPadrao(numElementos, somaElementos));
        System.out.println("Média ponderada: " + mediaPonderada(numElementos, somaElementos, elementos, pesoElementos));
        sc.close();
    }

    public static int mediaPadrao(int numElementos, int soma){
        return soma / numElementos;
    }

    public  static double mediaPonderada(int numElementos, int soma, int[] elementos, int[] pesos){
        double mediaPonderada;
        int somaPesos = Arrays.stream(pesos).sum();
        double elementosComPeso = 0;
        for (int i = 0; i < numElementos; i++){
            elementosComPeso += elementos[i] * pesos[i];
        }
        mediaPonderada = elementosComPeso / somaPesos;

        return mediaPonderada;
    }
}
