package ListaRevisao.Encapsulamento.Ex02;

public class CircuitoEletronico {
    private double tensao;
    private double corrente;

    public void setCorrente(double corrente) {
        this.corrente = corrente;
    }

    public double getCorrente() {
        return corrente;
    }

    public void setTensao(double tensao) {
        this.tensao = tensao;
    }

    public double getTensao() {
        return tensao;
    }

    public double calcResistencia(double tensao, double corrente){
        return tensao / corrente;
    }

    public double calcPotDissipada(double resistencia, double corrente){
        double U = resistencia * corrente;
        return U * corrente;
    }
}
