package ListaRevisao.Encapsulamento.Ex02;

import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        CircuitoEletronico circuito = new CircuitoEletronico();
        double resistencia;
        double potDissipada;
        System.out.println("Circuito Eletrônico!");
        System.out.print("Insira a tensão do circuito eletrônico: ");
        circuito.setTensao(sc.nextDouble());
        System.out.print("Insira a corrente do circuito eletrônico: ");
        circuito.setCorrente(sc.nextDouble());
        resistencia = circuito.calcResistencia(circuito.getTensao(), circuito.getCorrente());
        potDissipada = circuito.calcPotDissipada(resistencia, circuito.getCorrente());
        System.out.printf("\nCircuito: \n1.Tensão: %.2f \n2. Corrente: %.2f \n3. Resistencia %.2f \n4. Potência Dissipada: %.2f", circuito.getTensao(), circuito.getCorrente(), resistencia, potDissipada);

    }
}
