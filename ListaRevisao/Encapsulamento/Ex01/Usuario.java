package ListaRevisao.Encapsulamento.Ex01;

import java.util.Objects;

public class Usuario {
    private String nome;
    private String email;
    private String senha;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public boolean conferirSenha(String senhaLocal){
        if (Objects.equals(senha, senhaLocal)){
            return true;
        } else {
            return false;
        }
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
