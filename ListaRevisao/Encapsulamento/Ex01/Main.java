package ListaRevisao.Encapsulamento.Ex01;

import java.util.Objects;
import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        //Instância de um scanner de leitura
        Scanner sc = new Scanner(System.in);

        //Instância do usuário
        Usuario user = new Usuario();

        System.out.print("\nInsira o email do usuário: ");
        user.setEmail(sc.nextLine());

        System.out.print("Defina a senha do usuário: ");
        user.setSenha(sc.nextLine());

        System.out.print("Insira o nome do usuário: ");
        user.setNome(sc.nextLine());

        //Testando as informações
        System.out.println("\nOk, vamos fazer o login!");
        String emailLocal;
        String senhaLocal;
        System.out.print("Digite o email do usuário: ");
        emailLocal = sc.nextLine();
        System.out.print("Insira a senha do usuário: ");
        senhaLocal = sc.nextLine();
        if (Objects.equals(emailLocal, user.getEmail()) && user.conferirSenha(senhaLocal)){
            System.out.println("\nÉ você mesmo!");
            System.out.printf("\nDados básicos: \nNome: %s \nEmail: %s", user.getNome(), user.getEmail());
        } else {
            System.out.println("Senha ou Usuário incorretos!");
        }



    }
}
