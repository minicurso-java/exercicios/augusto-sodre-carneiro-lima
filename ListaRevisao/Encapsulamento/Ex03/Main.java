package ListaRevisao.Encapsulamento.Ex03;

import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        ContaCorrente contaAtual = new ContaCorrente();
        int resp = 0;

        System.out.print("Defina o nome do titular da conta: ");
        contaAtual.setTitular(sc.nextLine());


        while (resp != 5){
            System.out.println("\n\nDigite o que deseja fazer: ");
            System.out.print("1. Depositar dinheiro \n2. Sacar dinheiro \n3. Mostrar saldo atual \n4. Mostrar nome do titular \nResposta: ");
            resp = sc.nextInt();
            switch (resp){
                case 1:
                    System.out.print("Digite a quantia a ser depositada: ");
                    contaAtual.deposito(sc.nextDouble());
                break;

                case 2:
                    System.out.print("Digite a quantia a ser sacada: ");
                    contaAtual.saque(sc.nextDouble());
                break;

                case 3:
                    System.out.printf("Saldo atual: %.2f", contaAtual.getSaldo());
                break;

                case 4:
                    System.out.printf("Nome do titular: %s", contaAtual.getTitular());
                break;

                default:
                    System.out.println("Número inválido!");
                break;
            }
        }
    }
}
