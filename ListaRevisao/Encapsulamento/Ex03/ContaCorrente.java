package ListaRevisao.Encapsulamento.Ex03;

public class ContaCorrente {
    private double saldo = 0;
    private String titular = "Maya JS";

    public void deposito(double deposito){
        saldo += deposito;
    }

    public void saque(double saque){
        if (saque <= saldo){
            saldo -= saque;
        } else {
            System.out.print("Valor indisponível baseado no saldo atual!");
        }
    }

    public double getSaldo() {
        return saldo;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public String getTitular() {
        return titular;
    }
}
