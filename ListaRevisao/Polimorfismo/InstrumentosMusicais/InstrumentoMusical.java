package ListaRevisao.Polimorfismo.InstrumentosMusicais;

public interface InstrumentoMusical {
    void afinar();
    String tocarNotaMusical(String nota);
}
