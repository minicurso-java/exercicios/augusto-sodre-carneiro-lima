package ListaRevisao.Polimorfismo.InstrumentosMusicais;

public class Piano implements InstrumentoMusical{
    @Override
    public void afinar() {
        System.out.println("Plim! Plim! Afinando o piano!");
    }

    @Override
    public String tocarNotaMusical(String nota) {
        return ("Teclando " + nota);
    }
}
