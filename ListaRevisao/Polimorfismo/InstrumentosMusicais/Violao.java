package ListaRevisao.Polimorfismo.InstrumentosMusicais;

public class Violao implements InstrumentoMusical {
    @Override
    public void afinar() {
        System.out.println("Ueum! Ueum! Afinando o violão!");
    }

    @Override
    public String tocarNotaMusical(String nota) {
        return ("Dedilhando " + nota);
    }
}
