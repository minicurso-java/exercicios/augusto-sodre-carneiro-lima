package ListaRevisao.Polimorfismo.InstrumentosMusicais;

public class Flauta implements InstrumentoMusical {
    @Override
    public void afinar() {
        System.out.println("Fiu! Fiu! Afinando a flauta!");
    }

    @Override
    public String tocarNotaMusical(String nota) {
        return ("Soprando " + nota);
    }
}
