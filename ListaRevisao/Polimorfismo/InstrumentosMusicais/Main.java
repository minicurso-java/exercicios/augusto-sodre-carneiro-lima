package ListaRevisao.Polimorfismo.InstrumentosMusicais;

import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        //Instanciando Scanner de leitura
        Scanner sc = new Scanner(System.in);

        //Instanciando os instrumentos a serem utilizados
        Flauta flauta = new Flauta();
        Piano piano = new Piano();
        Violao violao = new Violao();

        //Parte da flauta
        flauta.afinar();
        System.out.print("Defina qual nota ela irá tocar: ");
        System.out.println(flauta.tocarNotaMusical(sc.nextLine()));

        //Parte do piano
        piano.afinar();
        System.out.print("Defina qual nota ele irá tocar: ");
        System.out.println(piano.tocarNotaMusical(sc.nextLine()));

        //Parte do violao
        violao.afinar();
        System.out.print("Definal qual nota ele irá tocar: ");
        System.out.println(violao.tocarNotaMusical(sc.nextLine()));
    }
}
