package ListaRevisao.Polimorfismo.Veiculos;

import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        //Instanciando Scanner
        Scanner sc = new Scanner(System.in);

        //Instanciando veiculos
        Caminhao caminhao = new Caminhao();
        Carro carro = new Carro();
        Moto moto = new Moto();

        //Fazendo parte do caminhão
        System.out.println("\nCaminhão!");
        System.out.println("Velocidade inicial: " + caminhao.velocidadeAtual + " km/h");
        System.out.print("Vamos acelerar! Insira quantos km/h deseja adicionar: ");
        caminhao.acelerar(sc.nextDouble());
        sc.nextLine();
        System.out.print("Deseja frear? [s/n]: ");
        String resp = sc.nextLine();
        if (resp.charAt(0) == 's'){
            caminhao.frear();
        }
        System.out.print("Digite qual a direção do caminhão: ");
        caminhao.virar(sc.nextLine());
        System.out.printf("\nCaminhão indo a %.2f km/h e na direção %s", caminhao.velocidadeAtual, caminhao.direcao);

        //Fazendo parte do Carro
        System.out.println("\nCarro!");
        System.out.println("Velocidade inicial: " + carro.velocidadeAtual + " km/h");
        System.out.print("Vamos acelerar! Insira quantos km/h deseja adicionar: ");
        carro.acelerar(sc.nextDouble());
        sc.nextLine();
        System.out.print("Deseja frear? [s/n]: ");
        resp = sc.nextLine();
        if (resp.charAt(0) == 's'){
            carro.frear();
        }
        System.out.print("Digite qual a direção do carro: ");
        carro.virar(sc.nextLine());
        System.out.printf("\nCarro indo a %.2f km/h e na direção %s", carro.velocidadeAtual, carro.direcao);

        //Fazendo parte da Moto
        System.out.println("\nMoto!");
        System.out.println("Velocidade inicial: " + moto.velocidadeAtual + " km/h");
        System.out.print("Vamos acelerar! Insira quantos km/h deseja adicionar: ");
        moto.acelerar(sc.nextDouble());
        sc.nextLine();
        System.out.print("Deseja frear? [s/n]: ");
        resp = sc.nextLine();
        if (resp.charAt(0) == 's'){
            moto.frear();
        }
        System.out.print("Digite qual a direção da moto: ");
        moto.virar(sc.nextLine());
        System.out.printf("\nMoto indo a %.2f km/h e na direção %s", moto.velocidadeAtual, moto.direcao);

    }
}
