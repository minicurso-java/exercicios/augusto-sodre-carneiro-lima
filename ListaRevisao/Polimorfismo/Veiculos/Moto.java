package ListaRevisao.Polimorfismo.Veiculos;

public class Moto implements Veiculo {
    double velocidadeAtual = 75.5;
    String direcao = "";

    @Override
    public void acelerar(double velocidade) {
        velocidadeAtual += velocidade;
    }

    @Override
    public void frear() {
        velocidadeAtual = 0;
    }

    @Override
    public void virar(String direcao) {
        this.direcao = direcao;
    }
}
