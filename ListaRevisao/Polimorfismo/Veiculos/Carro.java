package ListaRevisao.Polimorfismo.Veiculos;

public class Carro implements Veiculo {
    double velocidadeAtual = 10;
    String direcao = "";

    @Override
    public void acelerar(double velocidade) {
        velocidadeAtual += velocidade;
    }

    @Override
    public void frear() {
        velocidadeAtual = 0;
    }

    @Override
    public void virar(String direcao) {
        this.direcao = direcao;
    }
}
