package ListaRevisao.Polimorfismo.Veiculos;

public interface Veiculo {
    void acelerar(double velocidade);
    void frear();
    void virar(String direcao);
}
