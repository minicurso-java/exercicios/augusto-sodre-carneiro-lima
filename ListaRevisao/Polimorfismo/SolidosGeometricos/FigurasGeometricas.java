package ListaRevisao.Polimorfismo.SolidosGeometricos;

public interface FigurasGeometricas {
    abstract double calcArea();

    abstract double calcPerimetro();
}
