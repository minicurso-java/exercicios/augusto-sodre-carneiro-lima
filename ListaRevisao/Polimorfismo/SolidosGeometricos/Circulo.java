package ListaRevisao.Polimorfismo.SolidosGeometricos;

public class Circulo implements FigurasGeometricas {
    double raio;

    @Override
    public double calcArea() {
        return Math.PI * Math.pow(raio, 2);
    }

    public double calcPerimetro(){
        return 2.0 * Math.PI * raio;
    }
}
