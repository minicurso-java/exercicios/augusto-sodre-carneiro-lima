package ListaRevisao.Polimorfismo.SolidosGeometricos;

public class Retangulo implements FigurasGeometricas {
    double altura;
    double largura;

    @Override
    public double calcArea() {
        return altura * largura;
    }

    @Override
    public double calcPerimetro() {
        return (altura * 2.0) + (largura * 2.0);
    }
}
