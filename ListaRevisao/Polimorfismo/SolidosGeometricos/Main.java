package ListaRevisao.Polimorfismo.SolidosGeometricos;

import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        //Instâncias das classes
        Circulo circulo = new Circulo();
        Quadrado quadrado = new Quadrado();
        Retangulo retangulo = new Retangulo();
        Triangulo triangulo = new Triangulo();

        System.out.print("\nSólidos geométricos!\n");

        System.out.print("\nCírculo!\n");
        System.out.print("Digite o raio do círculo: ");
        circulo.raio = sc.nextDouble();
        System.out.printf("\n-Área: %.3f \n-Perímetro: %.3f\n", circulo.calcArea(), circulo.calcPerimetro());

        System.out.print("\nQuadrado!\n");
        System.out.print("Digite o lado do quadrado: ");
        quadrado.lado = sc.nextDouble();
        System.out.printf("\n-Área: %.3f \n-Perímetro: %.3f\n", quadrado.calcArea(), quadrado.calcPerimetro());

        System.out.print("\nRetângulo!\n");
        System.out.print("Digite a altura do retângulo: ");
        retangulo.altura = sc.nextDouble();
        System.out.print("Digite a largura do retângulo: ");
        retangulo.largura = sc.nextDouble();
        System.out.printf("\n-Área: %.3f \n-Perímetro: %.3f\n", retangulo.calcArea(), retangulo.calcPerimetro());

        System.out.print("\nTriângulo!\n");
        for (int i = 0; i < 3; i++){
            System.out.printf("Digite o lado %d do triângulo: ", i + 1);
            triangulo.lados[i] = sc.nextDouble();
        }
        System.out.printf("\n-Área: %.3f \n-Perímetro: %.3f\n", triangulo.calcArea(), triangulo.calcPerimetro());
        System.out.print("\nBye bye!\n");
    }
}
