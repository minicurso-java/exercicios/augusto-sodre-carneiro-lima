package ListaRevisao.Polimorfismo.SolidosGeometricos;

public class Quadrado implements FigurasGeometricas {
    double lado;

    @Override
    public double calcArea() {
        return Math.pow(lado,2);
    }

    @Override
    public double calcPerimetro() {
        return 4.0 * lado;
    }
}
