package ListaRevisao.Heranca.Ex02;

public class Eletronico extends Produto{
    private String sistOp;

    void setNome(String nome) {
        this.nome = nome;
    }

    void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    void setPreco(double preco) {
        this.preco = preco;
    }

    void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    void setSistOp(String sistOp) {
        this.sistOp = sistOp;
    }

    String getSistOp() {
        return sistOp;
    }
}
