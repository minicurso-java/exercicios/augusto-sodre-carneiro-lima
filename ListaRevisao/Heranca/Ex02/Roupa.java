package ListaRevisao.Heranca.Ex02;

public class Roupa extends Produto {
    private String cor;

    void setNome(String nome) {
        this.nome = nome;
    }

    void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    void setPreco(double preco) {
        this.preco = preco;
    }

    void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    void setCor(String cor) {
        this.cor = cor;
    }

    String getCor(){
        return cor;
    }
}
