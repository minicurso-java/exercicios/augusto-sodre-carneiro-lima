package ListaRevisao.Heranca.Ex02;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
      //Instância do Scanner para leitura
      Scanner sc = new Scanner(System.in);

      //Instância de objetos
        Livro livro = new Livro();
        Eletronico eletronico = new Eletronico();
        Roupa roupa = new Roupa();

        //Preenchendo livro
        System.out.print("\nLivro!");
        System.out.print("\nDigite o nome: ");
        livro.setNome(sc.nextLine());
        System.out.print("\nDigite a descrição: ");
        livro.setDescricao(sc.nextLine());
        System.out.print("\nDigite o nome do autor: ");
        livro.setAutor(sc.nextLine());
        System.out.print("\nDigite a quantidade: ");
        livro.setQuantidade(sc.nextInt());
        sc.nextLine();
        System.out.print("\nDigite o preço: ");
        livro.setPreco(sc.nextDouble());
        sc.nextLine();

        //Preenchendo eletronico
        System.out.print("\nEletrônico!");
        System.out.print("\nDigite o nome: ");
        eletronico.setNome(sc.nextLine());
        System.out.print("\nDigite a descrição: ");
        eletronico.setDescricao(sc.nextLine());
        System.out.print("\nDigite o sistema operacional: ");
        eletronico.setSistOp(sc.nextLine());
        System.out.print("\nDigite a quantidade: ");
        eletronico.setQuantidade(sc.nextInt());
        sc.nextLine();
        System.out.print("\nDigite o preço: ");
        eletronico.setPreco(sc.nextDouble());
        sc.nextLine();

        //Preenchendo Roupa
        System.out.print("\nRoupa!");
        System.out.print("\nDigite o nome: ");
        roupa.setNome(sc.nextLine());
        System.out.print("\nDigite a descrição: ");
        roupa.setDescricao(sc.nextLine());
        System.out.print("\nDigite a cor: ");
        roupa.setCor(sc.nextLine());
        System.out.print("\nDigite a quantidade: ");
        roupa.setQuantidade(sc.nextInt());
        sc.nextLine();
        System.out.print("\nDigite o preço: ");
        roupa.setPreco(sc.nextDouble());
        sc.nextLine();

        //Output das informações
      System.out.println("\nResumo do estoque");
      System.out.println("Livro: " + livro.nome + "\nAutor: " + livro.getAutor() + " \nPreço do estoque (R$): " + (livro.preco * livro.quantidade) + "\n");
      System.out.println("Eletrônico: " + eletronico.nome + "\nSO: " + eletronico.getSistOp() + " \nPreço do estoque (R$): " + (eletronico.preco * eletronico.quantidade) + "\n");
      System.out.println("Roupa: " + roupa.nome + "\nCor: " + roupa.getCor() + " \nPreço do estoque (R$): " + (roupa.preco * roupa.quantidade) + "\n");
    }
}
