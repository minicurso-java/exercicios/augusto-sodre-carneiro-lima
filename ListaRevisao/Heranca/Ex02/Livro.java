package ListaRevisao.Heranca.Ex02;

public class Livro extends Produto {
    private String autor;

    void setNome(String nome) {
        this.nome = nome;
    }

    void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    void setPreco(double preco) {
        this.preco = preco;
    }

    void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    void setAutor(String autor) {
        this.autor = autor;
    }

    String getAutor(){
        return autor;
    }
}
