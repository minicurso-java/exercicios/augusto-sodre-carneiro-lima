package ListaRevisao.Heranca.Ex01;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        //Instância do Scanner de leitura
        Scanner sc = new Scanner(System.in);

        //Instância dos objetos
        Smartphone smartphone = new Smartphone();
        Notebook notebook = new Notebook();
        Tablet tablet = new Tablet();

        //Preenchendo smartphone
        System.out.print("\nDigite a marca do smartphone: ");
        smartphone.setMarca(sc.next());
        System.out.print("Digite o ano de fabricação do smartphone: ");
        smartphone.setAnoFabric(sc.nextInt());

        //Preenchendo tablet
        System.out.print("\nDigite a marca do tablet: ");
        tablet.setMarca(sc.next());
        System.out.print("Digite o ano de fabricação do tablet: ");
        tablet.setAnoFabric(sc.nextInt());

        //Preenchendo notebook
        System.out.print("\nDigite a marca do notebook: ");
        notebook.setMarca(sc.next());
        System.out.print("Digite o ano de fabricação do notebook: ");
        notebook.setAnoFabric(sc.nextInt());

        //Output de tudo
        System.out.println("\nSmartphone: \n-Marca: " + smartphone.marca + " \n-Ano fabricação: " + smartphone.anoFabricacao);
        System.out.println("\nTablet: \n-Marca: " + tablet.marca + " \n-Ano fabricação: " + tablet.anoFabricacao);
        System.out.println("\nNotebook: \n-Marca" + notebook.marca + " \n-Ano fabricação: " + notebook.anoFabricacao);

    }
}
