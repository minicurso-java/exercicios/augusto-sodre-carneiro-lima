package ListaRevisao.Heranca.Ex03.Animais;

import ListaRevisao.Heranca.Ex03.Base.Reptil;

public class Iguana extends Reptil {
    public static void main(String[] args){
        Reptil iguana = new Reptil();
        iguana.nome = "Joseph";
        iguana.idade = 16;
        System.out.printf("\nA iguana %s possui %d anos. %s\n", iguana.nome, iguana.idade, iguana.descricao);
        iguana.cortarRabo();
    }
}
