package ListaRevisao.Heranca.Ex03.Animais;

import ListaRevisao.Heranca.Ex03.Base.Ave;

public class Pardal extends Ave {
    public static void main(String[] args){
        Ave pardal = new Ave();
        pardal.nome = "Jacob";
        System.out.printf("\nO pardalzinho %s possui %d anos. %s\n", pardal.nome, pardal.idade, pardal.descricao);
        pardal.voar();
    }
}
