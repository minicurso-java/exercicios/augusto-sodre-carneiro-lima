package ListaRevisao.Heranca.Ex03.Animais;

import ListaRevisao.Heranca.Ex03.Base.Mamifero;

public class Gato extends Mamifero {
    public static void main(String[] args){
        Mamifero gato = new Mamifero();
        gato.nome = "Tereza";
        gato.idade = 120;
        System.out.printf("\nA gata %s possui %d anos. %s\n", gato.nome, gato.idade, gato.descricao);
        gato.amamentar();
    }
}
