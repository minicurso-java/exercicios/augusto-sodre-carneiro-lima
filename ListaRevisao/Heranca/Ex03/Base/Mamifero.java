package ListaRevisao.Heranca.Ex03.Base;

public class Mamifero extends Animal {
    public String descricao = "Os mamíferos constituem uma Subclasse de animais vertebrados do domínio Eukaryota, do reino Animalia e filo Chordata.";

    public void amamentar(){
        System.out.println("Amamentando!");
    }
}
