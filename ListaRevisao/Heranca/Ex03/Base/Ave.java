package ListaRevisao.Heranca.Ex03.Base;

public class Ave extends Animal{
    public String descricao = "Aves são uma classe de seres vivos vertebrados endotérmicos caracterizada pela presença de penas, um bico sem dentes, oviparidade, elevado metabolismo e um esqueleto pneumático leve.";

    public void voar(){
        System.out.println("Voando yeah!");
    }
}
