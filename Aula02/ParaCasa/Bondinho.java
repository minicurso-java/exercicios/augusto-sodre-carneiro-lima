package Aula02.ParaCasa;

import java.util.Scanner;

public class Bondinho {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int numAlunos, numMonitores;
        char resultado;
        numAlunos = sc.nextInt();
        numMonitores = sc.nextInt();

        if (numAlunos + numMonitores <= 50){
            resultado = 'S';
        } else {
            resultado = 'N';
        }
        System.out.println(resultado);
        sc.close();
    }
}