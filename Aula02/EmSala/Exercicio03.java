package Aula02.EmSala;

import java.util.Scanner;
import java.util.Locale;

public class Exercicio03 {

	public static void main(String[] args) {
		Locale.setDefault(Locale.ENGLISH);
		Scanner sc = new Scanner(System.in);
		int id, workedHours; 
		double hourlyPayment, salary;
		id = sc.nextInt();
		workedHours = sc.nextInt();
		hourlyPayment = sc.nextDouble();
		salary = workedHours * hourlyPayment;
		System.out.printf("Funcionário: %d\n", id);
		System.out.printf("Salário: U$%.2f", salary);
		sc.close();
	}

}
