package Aula01;

//Exercício 01 da Aula 01

import java.util.Scanner;

public class Exercicio01 {

	public static void main(String[] args) {
		double a, b, c;
		double resultLocal;
		Scanner sc = new Scanner(System.in);
		a = sc.nextDouble();
		b = sc.nextDouble();
		c = sc.nextDouble();
		resultLocal = a*c/2.0;
		System.out.printf("Triângulo: %.3f\n", resultLocal);
		resultLocal = 3.14159 * Math.pow(c, 2.0);
		System.out.printf("Círculo: %.3f\n", resultLocal);
		resultLocal = (a+b)/2.0*c;
		System.out.printf("Trapézio: %.3f\n", resultLocal);
		resultLocal = Math.pow(b, 2.0);
		System.out.printf("Quadrado: %.3f\n", resultLocal);
		resultLocal = a * b;
		System.out.printf("Retângulo: %.3f\n", resultLocal);
		sc.close();
	}

}
