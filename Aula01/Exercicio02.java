package Aula01;

//Exercício 02 da Aula 01

import java.util.Scanner;

public class Exercicio02 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int a, b, c, d, result;
		a = sc.nextInt();
		b = sc.nextInt();
		c = sc.nextInt();
		d = sc.nextInt();
		result = (a * b - c * d);
		System.out.printf("Resultado: %d", result);
		sc.close();
	}

}
