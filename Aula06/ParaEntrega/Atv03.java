package Aula06.ParaEntrega;

import java.util.Arrays;
import java.util.Scanner;

public class Atv03 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.print("Digite o número de primos que deseja ver: ");
        int num = sc.nextInt();
        int contadNumeros = 0;
        int indexLista = 0;
        int[] lista = new int[num];
        for (int i = 1; ;i++){
            int contDivisiores = 0;
            for (int j = 1; j <= i; j++){
                if (i%j == 0){
                    contDivisiores += 1;
                }
            }
            if (contDivisiores == 2){
                lista[indexLista] = i;
                indexLista++;
                contadNumeros++;
            }
            if (contadNumeros == num){
                break;
            }
        }

        System.out.println("Lista: " + Arrays.toString(lista));
        sc.close();
    }
}
