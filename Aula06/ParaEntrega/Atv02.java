package Aula06.ParaEntrega;

import java.util.Arrays;
import java.util.Scanner;

public class Atv02 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int[] lista = {1, 2, 3, 4, 5, 6, 7, 8};
        int index1, index2, num1, num2, numTemp;
        System.out.printf("Digite o primeiro índice para troca: ");
        index1 = sc.nextInt();
        num1 = lista[index1];
        System.out.printf("Digite o segundo índice para troca: ");
        index2 = sc.nextInt();
        num2 = lista[index2];

        numTemp = num1;
        num1 = num2;
        num2 = numTemp;
        lista[index1] = num1;
        lista[index2] = num2;
        System.out.println("Lista: " + Arrays.toString(lista));
        sc.close();

    }
}
