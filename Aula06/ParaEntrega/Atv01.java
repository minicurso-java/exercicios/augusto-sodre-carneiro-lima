package Aula06.ParaEntrega;

import java.util.Arrays;
import java.util.Scanner;

public class Atv01 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int[] list = new int[7];
        System.out.printf("Digite o primeiro elemento do array: ");
        list[0] = sc.nextInt();
        System.out.printf("Digite o último elemento do array: ");
        list[6] = sc.nextInt();
        System.out.println("Lista: " + Arrays.toString(list));
        sc.close();
    }
}
