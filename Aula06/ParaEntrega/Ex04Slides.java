package Aula06.ParaEntrega;

import java.util.Scanner;

public class Ex04Slides {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int numElementos;
        System.out.print("Insira o número de elementos: ");
        numElementos = sc.nextInt();
        double [] listaElementos = new double[numElementos];
        double maiorNumero = -1000000000.0;
        int indexMaiorNumero = 0;
        for (int i = 0; i < numElementos; i++){
            System.out.printf("Insira o elemento da posição %d\n", i);
            listaElementos[i] = sc.nextInt();
            if (listaElementos[i] >= maiorNumero){
                maiorNumero = listaElementos[i];
                indexMaiorNumero = i;
            }
        }
        System.out.printf("\nMaior número digitado: %.2f \nPosição do número: %d", maiorNumero, indexMaiorNumero);
        sc.close();
    }
}
