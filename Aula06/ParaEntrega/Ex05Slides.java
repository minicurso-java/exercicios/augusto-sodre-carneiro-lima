package Aula06.ParaEntrega;

import java.util.Scanner;

public class Ex05Slides {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int numPessoas;
        System.out.print("Digite o número de pessoas: ");
        numPessoas = sc.nextInt();
        String[] nomes = new String[numPessoas];
        String nomeMaisVelho = "";
        int[] idades = new int[numPessoas];
        int idadeMaisVelho = -1;
        for (int i = 0; i < numPessoas; i++){
            System.out.printf("Nome da pessoa %d: ", i + 1);
            nomes[i] = sc.next();
            System.out.printf("Idade da pessoa %d: ", i + 1);
            idades[i] = sc.nextInt();
            if (idades[i] >= idadeMaisVelho){
                idadeMaisVelho = idades[i];
                nomeMaisVelho = nomes[i];
            }
            System.out.printf("\n");
        }
        System.out.printf("Nome do mais velho: %s \nSua idade: %d", nomeMaisVelho, idadeMaisVelho);
        sc.close();
    }
}
