package Aula07;

import java.util.Arrays;
import java.util.Scanner;

public class TrocoEmMoedas {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int centavos = sc.nextInt();
        int totalMoedas = 0;
        int[] moedas = new int[6];
        int moedaAtual = 100;
        for (int i = 0; i < 6; i++){
            if (i < 2){
                moedas[i] = centavos/moedaAtual;
                centavos = centavos%moedaAtual;
                moedaAtual /= 2;
            } else if (i == 2) {
                moedas[i] = centavos/moedaAtual;
                centavos = centavos%moedaAtual;
                moedaAtual = 10;
            } else if (i <= 4){
                moedas[i] = centavos/moedaAtual;
                centavos = centavos%moedaAtual;
                moedaAtual /= 2;
            } else {
                moedaAtual = 1;
                moedas[i] = centavos/moedaAtual;
            }
        }
        totalMoedas = Arrays.stream(moedas).sum();

        System.out.println(totalMoedas);
        for (int i = 0; i < 6; i++){
            System.out.println(moedas[i] + " ");
        }
        sc.close();
    }
}
