package Aula07;

import java.util.Arrays;
import java.util.Scanner;

public class OrdenacaoSimples {
    public static void main(String [] args){
        Scanner sc = new Scanner(System.in);
        int numElementos = sc.nextInt();
        int[] lista = new int[numElementos];
        for (int i = 0; i < numElementos; i++){
            lista[i] = sc.nextInt();
        }

        for (int i = 0; i < numElementos - 1; i++){
            for (int j = 0; j < numElementos - 1; j++){
                int numTemp = 0;
                if (lista[j] > lista[j+1]){
                    numTemp = lista[j+1];
                    lista[j+1] = lista[j];
                    lista[j] = numTemp;
                }
            }
        }

        for (int i = 0; i < numElementos; i++){
            System.out.print(lista[i] + " ");
        }
        sc.close();
    }
}
