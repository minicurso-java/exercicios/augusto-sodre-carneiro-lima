package Aula07;

import java.util.Arrays;
import java.util.Scanner;

public class QuadradoMagico {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int[][] matriz= new int[3][3];
        int diagonal1 = 0, diagonal2 = 0;
        int[] linhas  = new int[3];
        int[] colunas = new int[3];
        System.out.println("Preencha a matriz linha por linha: ");
        for (int i = 0; i < 3; i++){
            for (int j = 0; j < 3; j++){
                matriz[i][j] = sc.nextInt();
                //Já faz a soma da diagonal principal
                if (i == j){
                    diagonal1 += matriz[i][j];
                }
                //Já faz a soma das linhas 0, 1 e 2 (Que acabaram de ser preenchidas)
                linhas[i] += matriz[i][j];
            }

        }
        System.out.println("Linhas: " + Arrays.toString(linhas));
        System.out.println("Diagonal Principal: " + diagonal1);

        //Cálculo das colunas
        for (int i = 0; i < 3; i++){
            for (int j = 0; j < 3; j++){
                colunas[i] += matriz[j][i];
            }
        }
        System.out.println("Colunas: " + Arrays.toString(colunas));

        //Cálcula da diagonal secundária
        int contColuna = 2;
        for (int i = 0; i < 3; i++){
            diagonal2 += matriz[i][contColuna];
            contColuna--;
        }
        System.out.println("Diagonal Secundária: " + diagonal2);

        //Verificação de Quadrado Mágico e Output
        boolean linhasIguais = false;
        boolean colunasIguais = false;
        boolean diagonaisIguais = false;
        if (linhas[0] == linhas[1] && linhas[1] == linhas[2]){
            linhasIguais = true;
        }
        if (colunas[0] == colunas[1] && colunas[1] == colunas[2]){
            colunasIguais = true;
        }
        if (diagonal1 == diagonal2){
            diagonaisIguais = true;
        }

        System.out.println();
        if (linhasIguais && colunasIguais && diagonaisIguais){
            System.out.println("É uma quadrado mágico!");
        } else {
            System.out.println("Não é um quadrado mágico!");
        }

    }
}
