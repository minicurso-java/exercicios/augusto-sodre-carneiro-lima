package Aula03.Em_Sala;

import java.util.Locale;
import java.util.Scanner;

public class Ex04 {

	public static void main(String[] args) {
		Locale.setDefault(Locale.ENGLISH);
		Scanner sc = new Scanner(System.in);
		double nota = sc.nextDouble();
		if (nota >= 5) {
			System.out.println("Aprovado!");
		} else {
			System.out.println("Reprovado");
		}
		sc.close();
	}
}
