package Aula03.Em_Sala;

import java.util.Scanner;

public class Ex02 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int num;
		System.out.print("Insira um número: ");
		num = sc.nextInt();
		if (num != 10) {
			num = 10;
		}
		sc.close();
	}
}
