package Aula03.Em_Sala;

import java.util.Scanner;

public class Ex06 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int horario = sc.nextInt();
		if (horario >= 0 && horario < 6) {
			System.out.printf("Agora são %d horas.Boa madrugada, Batman!");
		} else if (horario >= 6 && horario < 12) {
			System.out.printf("Agora são %d horas. Bom dia, o Sol já levantou na fazendinha!");
		} else if (horario >= 12 && horario < 18) {
			System.out.printf("Agora são %d horas. Boa tarde, my friend!", horario);
		} else if (horario >= 18 && horario <= 24) {
			System.out.printf("Agora são %d horas. Boa noite, buddy!", horario);		}
		sc.close();
	}

}
