package Aula03.Em_Sala;

import java.util.Scanner;

public class Ex05 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int tempoTotal, tempoExcedido;
		double preco;
		tempoTotal = sc.nextInt();
		tempoExcedido = tempoTotal - 100; 
		if (tempoExcedido > 0) {
			preco = 50.0 + (2.0 * tempoExcedido);
			System.out.println("Valor a pagar R$" + preco);
		} else {
			preco = 50;
			System.out.println("Valor a pagar R$" + preco);
		}
		sc.close();

	}

}
