package Aula03.Em_Sala;

import java.util.Scanner;

public class ExFunc01 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int num1, num2;
		num1 = sc.nextInt();
		num2 = sc.nextInt();
		System.out.println(multiplica(num1, num2));	
		sc.close();
	}
	
	public static int multiplica(int num1, int num2) {
		int result = num1 * num2;
		return result;
	}

}
