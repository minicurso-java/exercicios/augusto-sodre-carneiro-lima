package Aula03.Em_Sala;

import java.util.Scanner;

public class Ex01 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int num;
		System.out.println("Insira um número: ");
		num = sc.nextInt();
		if (num > 2) {
			System.out.println("Maior que dois!");
		} else if (num < 2) {
			System.out.println("Menor que dois!");
		} else {
			System.out.println("Igual a dois!");
		}
		sc.close();
	}
}
