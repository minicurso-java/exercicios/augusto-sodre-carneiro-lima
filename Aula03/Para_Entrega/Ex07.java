package Aula03.Para_Entrega;

import java.util.Scanner;

public class Ex07 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int operator, num1, num2;
		double result;
		System.out.print("Insira o primeiro operando: ");
		num1 = sc.nextInt();
		System.out.print("Insira o segundo operando: ");
		num2 = sc.nextInt();
		System.out.printf("Operação:\n1 - Soma\n2 - Subtração\n3 - Divisão\n4 - Multiplicação\nResposta: ");
		operator = sc.nextInt();
		switch (operator){
		case 1:
			result = num1 + num2;
			System.out.printf("Resultado: %.2f", result);
		break;
		
		case 2:
			result = num1 - num2;
			System.out.printf("Resultado: %.2f", result);
		break;
		case 3:
			result = num1 / num2;
			System.out.printf("Resultado: %.2f", result);
		break;
		
		case 4:
			result = num1 * num2;
			System.out.printf("Resultado: %.2f", result);
		}
		sc.close();
	}

}
