package Aula03.Para_Entrega;

import java.util.Scanner;

public class Ex08 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		float x, y;
		x = sc.nextFloat();
		y = sc.nextFloat();
		if (x == y && x == 0) {
			System.out.println("Está na origem!");
		} else if (x == 0 && y != 0) {
			System.out.println("Está no eixo Y");
		} else if (x != 0 && y == 0 ) {
			System.out.println("Está no eixo X");
		} else if (x > 0 && y > 0) {
			System.out.println("Está no primeiro quadrante!");
		} else if (x < 0 && y > 0) {
			System.out.println("Está no segundo quadrante!");
		} else if (x < 0 && y < 0) {
			System.out.println("Está no terceiro quadrante!");
		} else if (x > 0 && y < 0) {
			System.out.println("Está no quarto quadrante!");
		}
		

	}

}
