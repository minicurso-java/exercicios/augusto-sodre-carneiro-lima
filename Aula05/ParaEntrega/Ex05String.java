package Aula05.ParaEntrega;

import java.util.Scanner;

public class Ex05String {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        for (int i = 1; i <= num; i++){
            System.out.printf("%d  %.1f  %.1f \n", i, Math.pow(i, 2), Math.pow(i, 3));
        }
        sc.close();
    }
}
