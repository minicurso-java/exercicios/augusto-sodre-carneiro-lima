package Aula05.ParaEntrega;

import java.util.Scanner;

public class Ex05While {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        float c, f;
        char answer = 's';
        while (answer == 's') {
            System.out.print("Digite a temperatura em Celsius: ");
            c = sc.nextInt();
            f = 9 * c / 5 + 32;
            System.out.println("Equivalente em Fahrenteit: " + f);
            System.out.println("Deseja continuar? [s/n]");
            answer = sc.next().charAt(0);
        }

    }
}
