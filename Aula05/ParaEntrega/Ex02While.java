package Aula05.ParaEntrega;

import java.util.Scanner;

public class Ex02While {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int answer = 0, alcool = 0, gasolina = 0, diesel = 0;
        do {
            answer = sc.nextInt();
            switch (answer) {
                case 1:
                    alcool += 1;
                break;

                case 2:
                    gasolina += 1;
                break;

                case 3:
                    diesel += 1;
                break;

                case 4:
                break;

                default:
                    System.out.println("Digite um número válido!");
            }
        } while (answer != 4);
        System.out.printf("MUITO OBRIGADO! \nÁlcool: %d \nGasolina: %d \nDiesel: %d\n", alcool, gasolina, diesel);
    }
}
