package Aula05.EmSala;

import java.util.Scanner;

public class Ex01 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = 2002, answer = 0;
        while(answer != num){
            answer = sc.nextInt();
            if (answer == num){
                break;
            } else {
                System.out.println("Senha incorreta!");
            }
        }
        System.out.println("Senha correta!");
        sc.close();
    }
}
