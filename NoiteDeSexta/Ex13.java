package NoiteDeSexta;

import java.util.Scanner;

public class Ex13 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("Insira o capital inicial:");
        double capInicial = sc.nextDouble();
        System.out.println("Insira a taxa mensal da aplicação:");
        double taxa = sc.nextDouble();
        System.out.println("Insira o tempo, em meses, da aplicação:");
        int tempo = sc.nextInt();
        double juro = capInicial * taxa * tempo / 100.0;
        double montante = capInicial + juro;
        System.out.println("Montante: " + montante);
        sc.close();
    }
}
