package NoiteDeSexta;

import java.util.Scanner;

public class Ex19 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("Digite o raio da esfera: ");
        double raio = sc.nextDouble();
        double volume = 4.0/3.0 * Math.PI * Math.pow(raio, 3);
        System.out.println("Volume: "+ volume);
    }
}
