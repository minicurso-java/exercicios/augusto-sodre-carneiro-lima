package NoiteDeSexta;

import java.util.Scanner;

public class Ex18 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("Digite uma string: ");
        String answer = sc.next();
        System.out.println("Qual caractere deseja buscar:");
        char goal = sc.next().charAt(0);
        int contador = 0;
        for (int i = 0; i < answer.length(); i++){
            if (answer.charAt(i) == goal){
                contador++;
            }
        }
        System.out.println("Vezes: " + contador);

    }
}
