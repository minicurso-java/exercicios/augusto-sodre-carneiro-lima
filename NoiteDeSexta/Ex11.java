package NoiteDeSexta;

import java.util.Scanner;

public class Ex11 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();

        int contadorDivi = 0;
        for (int i = 2; i <= num;i++){
            if (num%i == 0){
                contadorDivi++;
            }
        }
        if (contadorDivi == 1){
            System.out.println("É primo!");
        } else {
            System.out.println("Não é primo!");
        }
        sc.close();
    }
}
