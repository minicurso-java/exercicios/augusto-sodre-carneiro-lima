package NoiteDeSexta;

import java.util.Scanner;

public class Ex04 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        int resultado = 1;
        for (int i = num; i >= 1; i--){
            resultado *= i;
        }
        System.out.println(resultado);
        sc.close();
    }
}
