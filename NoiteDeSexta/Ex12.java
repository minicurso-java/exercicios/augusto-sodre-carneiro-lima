package NoiteDeSexta;

import java.util.Scanner;

public class Ex12 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("Insira as notas:");
        double num1 = sc.nextDouble();
        double num2 = sc.nextDouble();
        double num3 = sc.nextDouble();
        System.out.println("Insira os pesos:");
        double peso1 = sc.nextDouble();
        double peso2 = sc.nextDouble();
        double peso3 = sc.nextDouble();
        double resultado = (num1 * peso1 + num2 * peso2 + num3 * peso3) / (peso1 + peso2 + peso3);
        System.out.println("Média ponderada: " + resultado);
        sc.close();
    }
}
