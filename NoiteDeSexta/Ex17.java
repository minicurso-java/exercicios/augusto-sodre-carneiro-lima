package NoiteDeSexta;

import java.util.Arrays;
import java.util.Scanner;

public class Ex17 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("Digite o intervalo, início e depois fim: ");
        int inicio = sc.nextInt();
        int fim = sc.nextInt();
        if (inicio > fim){
            System.out.println("Erro! Início deve ser menor que o fim!");
            return;
        }
        int[] primos = new int[(Math.abs(fim-inicio)) + 1];
        int indexPrimos = 0;
        for (int i = inicio; i <= fim; i++){
            int contDiv = 1;
            for (int j = 2; j <= i; j++){
                if (i%j == 0){
                    contDiv++;
                }
            }
            if (contDiv == 2){
                primos[indexPrimos] = i;
                indexPrimos++;
            }
        }

        System.out.println(Arrays.toString(primos));
    }
}
