package NoiteDeSexta;

import java.util.Scanner;

public class Ex15 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        int somaDiv = 1;
        for (int i = 2; i < num; i++){
            if (num%i == 0){
                somaDiv += i;
            }
        }
        if (somaDiv == num){
            System.out.println("É um número perfeito!");
        } else {
            System.out.println("Não é um número perfeito!");
        }
        sc.close();
    }
}
