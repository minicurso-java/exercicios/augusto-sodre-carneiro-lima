package NoiteDeSexta;

import java.util.Arrays;
import java.util.Scanner;

public class Ex07 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        String answer = sc.next();
        int length = answer.length();
        char[] strippedAnswer = new char[length];
        strippedAnswer = answer.trim().strip().toCharArray();
        char[] reversedAnswer = new char[length];
        for (int i = 0; i < length; i++){
            reversedAnswer[i] = strippedAnswer[length - 1 - i];
        }
        if (Arrays.equals(reversedAnswer, strippedAnswer)){
            System.out.println("É um palíndromo!");
        } else {
            System.out.println("Não é um palíndromo!");
        }
        sc.close();
    }
}
