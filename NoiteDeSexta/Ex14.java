package NoiteDeSexta;

import java.util.Scanner;

public class Ex14 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        String answer = sc.next();
        int contVogais = 0;
        for (int i = 0; i < answer.trim().length(); i++){
            switch (answer.trim().toLowerCase().charAt(i)){
                case 'a', 'e', 'i', 'o', 'u':
                    contVogais++;
            }
        }
        System.out.println("Vogais: " + contVogais);
        sc.close();
    }
}
