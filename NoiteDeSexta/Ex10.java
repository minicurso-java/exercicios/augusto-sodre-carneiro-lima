package NoiteDeSexta;

import java.util.Scanner;

public class Ex10 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        for (int i = 10; i >= 0; i--){
            int resultado = num * i;
            System.out.println(num + " X " + i + " = " + resultado);
        }
        sc.close();
    }
}
