package NoiteDeSexta;

import javax.sound.midi.Soundbank;
import java.util.Scanner;

public class Ex08 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.print("Digite a temperatura em celcius (°C): ");
        double celcius = sc.nextDouble();
        double fahrenheit = 1.8 * celcius + 32;
        System.out.println(fahrenheit + "°F");
        sc.close();
    }
}
