package NoiteDeSexta;

import java.util.Scanner;

public class Ex05 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int num1 = sc.nextInt();
        int num2 = sc.nextInt();
        if (num1 > num2){
            System.out.println(num1 + " É maior!");
        } else if (num2 > num1){
            System.out.println(num2 + " É maior!");
        } else {
            System.out.println("Mesmo valor!");
        }
        sc.close();
    }
}
