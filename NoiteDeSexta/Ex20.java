package NoiteDeSexta;

import java.util.Scanner;

public class Ex20 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("Digite o ano:");
        int year = sc.nextInt();
        //para anos no estilo 1xxx ou 2xxx
        if (year >= 1000){
            if (Integer.toString(year).charAt(2) == '0' && Integer.toString(year).charAt(3) == '0' && year%400 == 0){
                System.out.println("O ano é bissexto!");
            } else if (Integer.toString(year).charAt(3) != '0' && year%4 == 0){
                System.out.println("O ano é bissexto!");
            } else{
                System.out.println("O ano não é bissexto!");
            }
        //para anos no estilo 1xx
        } else if (year >= 10){
            if (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0)){
                System.out.println("O ano é bissexto!");
            } else{
                System.out.println("O ano não é bissexto!");
            }
        //para anos no estilo x
        } else{
            if (year % 4 == 0){
                System.out.println("O ano é bissexto!");
            } else {
                System.out.println("O ano não é bissexto!");
            }

        }
        sc.close();

    }
}
