package Aula09.EmSala.Abstracao;

public abstract class Animal {
    public abstract void comer();
    public abstract void correr();
    public abstract void fazerBarulho();
}
