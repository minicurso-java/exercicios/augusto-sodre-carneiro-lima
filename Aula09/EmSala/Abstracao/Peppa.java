package Aula09.EmSala.Abstracao;

public class Peppa extends Animal {
    public void comer() {
        System.out.print("Eu gosto de comer bolinhos!");
    }

    public void fazerBarulho() {
        System.out.println("Oi, eu sou a Peppa!");
    }

    public void correr() {
        System.out.println("Eu corro rápido!");
    }
}
