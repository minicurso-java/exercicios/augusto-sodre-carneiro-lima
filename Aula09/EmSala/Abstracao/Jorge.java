package Aula09.EmSala.Abstracao;

public class Jorge extends Animal  {
    public void comer() {
        System.out.println("Comendo chocolate!");
    }

    public void fazerBarulho() {
        System.out.println("Hihihi");
    }

    public void correr() {
        System.out.println("Eu corro rápido, sou brabo");
    }
}
