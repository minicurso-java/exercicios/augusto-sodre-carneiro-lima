package Aula09.ParaCasa;

import java.util.Scanner;

public class Produto {
    String nome;
    double preco;
    int quantEstoque;

    public void adicionarUnidades(int numUnidades){
        quantEstoque += numUnidades;
    }

    public void removerUnidades(int numUnidades){
        if (numUnidades <= quantEstoque){
            quantEstoque -= numUnidades;
        } else {
            System.out.println("Inválido, estoque insuficiente!");
        }
    }

    public double valorEstoque(){
        return preco * quantEstoque;
    }

    //Main para testar:

    /*public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int resp = 0;
        Produto produtoAtual = new Produto();
        System.out.print("Digite o nome do produto: ");
        produtoAtual.nome = sc.nextLine();
        System.out.print("Digite o preço do produto: ");
        produtoAtual.preco = sc.nextDouble();
        System.out.print("Digite a quantidade em estoque do produto: ");
        produtoAtual.quantEstoque = sc.nextInt();
        while (resp != 4){
            System.out.println("\nDigite a opção: \n1. Adicionar Unidades \n2. Remover unidades \n3. Mostrar informações \n4.Sair");
            resp = sc.nextInt();
            switch (resp){
                case 1:
                    System.out.print("Digite quantas unidades para adicionar: ");
                    int quant = sc.nextInt();
                    produtoAtual.adicionarUnidades(quant);
                    break;

                case 2:
                    System.out.println("Digite quantas unidades para remover");
                    quant = sc.nextInt();
                    produtoAtual.removerUnidades(quant);
                    break;

                case 3:
                    System.out.printf("\nProduto: %s \nPreço: %.2f \nQuant. Estoque: %d \nPreço do estoque: %.2f\n", produtoAtual.nome, produtoAtual.preco, produtoAtual.quantEstoque, produtoAtual.valorEstoque());

                break;

                case 4:
                    return;
                default:
                    System.out.println("Número inválido!");
                    break;
            }
        }
    }*/

}
