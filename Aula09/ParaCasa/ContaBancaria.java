package Aula09.ParaCasa;

import java.util.Scanner;

public class ContaBancaria {
    int numero = 123456;
    double saldo = 100000000;
    String titular = "Pablo Escobar Gaviria";
    Scanner sc = new Scanner(System.in);

    public void depositar(){ //Método para realizar depósito no saldo atual
        double deposito;
        System.out.printf("\nDigite quanto deseja depositar: ");
        deposito = sc.nextDouble();
        saldo += deposito;
    }

    public void saque(){ //Método para retirar dinheiro do saldo atual
        double saque;
        System.out.printf("\nDigite quanto deseja sacar: ");
        saque = sc.nextDouble();
        saldo -= saque;
    }

    public void verificarSaldo(){ //Print na tela do saldo atual
        System.out.printf("\nNome: %s \nNúmero da conta: %d \nSaldo atual: %.2f", titular, numero, saldo);
    }

    //Main para testar:

    /*public static void main(String[] args) {
        ContaBancaria x = new ContaBancaria();
        Scanner sc = new Scanner(System.in);
        int resposta = 0;

        while (resposta != 4) {
            System.out.print("\n\n==============\nO que deseja fazer? \n1. Depósito \n2. Saque \n3. Verificar Saldo \n4. Sair \nResposta: ");
            resposta = sc.nextInt();
            switch (resposta) {
                case 1:
                    x.depositar();
                    break;

                case 2:
                    x.saque();
                    break;

                case 3:
                    x.verificarSaldo();
                    break;

                case 4:
                    return;

                default:
                    System.out.println("Número inválido!");
                    break;
            }
        }

    }*/
}
