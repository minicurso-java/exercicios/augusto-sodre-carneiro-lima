package Aula09.ParaCasa;

import javax.sound.midi.Soundbank;
import java.util.Arrays;
import java.util.Scanner;

public class Aluno {
    String nome = "Augusto Sodré";
    int matricula = 22400000;
    double[] notas;
    double media;
    boolean aprovado = false;

    public void preencherNotas(int numNotas){ //Preenche o array de notas
        Scanner sc = new Scanner(System.in);
        notas = new double[numNotas];
        for (int i = 0; i < numNotas; i++){
            System.out.printf("Nota %d: ", i+1);
            notas[i] = sc.nextDouble();
        }
    }

    public double calcMedia(int numNotas){ //Calcula a média baseada no array de notas
        double somaNotas = Arrays.stream(notas).sum();
        return media = somaNotas/numNotas;

    }

    public boolean verifAprovacao(double notaMedia){ //Calcula a aprovação baseada no cálculo da média
        if (media >= notaMedia){
            return aprovado = true;
        } else {
            return aprovado = false;
        }
    }

    //Teste na Main:

    /*public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        Aluno aluno = new Aluno();
        System.out.print("Digite o nome do aluno: ");
        aluno.nome = sc.nextLine();
        System.out.println("Digite o número de notas: ");
        int numNotas = sc.nextInt();
        aluno.preencherNotas(numNotas);
        System.out.printf("Nome: %s\n", aluno.nome);
        System.out.printf("Média: %.2f\n", aluno.calcMedia(numNotas));
        System.out.println("Aprovado: " + aluno.verifAprovacao(6));
        sc.close();
    }*/


}
